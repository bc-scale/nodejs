'use strict';

require('dotenv').config();
const pckg = require('./package.json');
const express = require('express');
const cors = require('cors')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const path = require("path");
const nunjucks = require('nunjucks');

const app = express();
const port = 3001;
const environment = process.env.NODE_ENV || 'production';
const version = pckg.version;


/**
 * server settings and configuration
 */
app.use(cors());

app.use(fileUpload({
    createParentPath: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(path.join(__dirname, '/public')));

app.set('view engine', 'html');
nunjucks.configure(path.join(__dirname, '/src/views'), {
    autoescape: true,
    express: app
});

/**
 * run server
 */
app.listen(port, () => {
    console.info(`## Server startet!`);
    console.info(`App listening on port ${port}`);
    console.info(`Environment configured: ${environment}`);
    console.info(`Version: ${version}`);
});

/**
 * define routes
 */
app.get('/api/status', async (req, res) => {
    const Database = require('./src/models/MongoModel');
    const db = new Database('');

    try {
        let database = await db.ping();

        res.status(200).json({
            success: true,
            api: `running`,
            version,
            database
        });
    } catch (e) {
        res.status(200).json({
            success: true,
            api: `running`,
            version,
            database: false
        });
    }
});

app.use('/gallery', require('./src/routes/routes.gallery'));
app.use('/snapshot', require('./src/routes/routes.snapshot'));