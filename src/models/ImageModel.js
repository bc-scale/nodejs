'use strict';

const path = require('path');

class ImageModel {

    static UPLOAD_DIR = path.join(__dirname, '/../../uploads');

    /**
     * store image-file
     * @param path
     * @param name
     * @param snapshot
     * @returns {Promise<void>}
     */
    async upload(path, name, snapshot) {
        await snapshot.mv(ImageModel.UPLOAD_DIR + '/' + path + '/' + name);
    }

}

module.exports = ImageModel;