'use strict';

const uuid = require('uuid');
const DatabaseModel = require('./../models/MongoModel');

class GalleryController {

    /**
     * fetch list of galleries
     * @returns {Promise<*[]|undefined>}
     */
    static async list() {
        const db = new DatabaseModel('galleries');
        return await db.aggregate([]);
    }


    /**
     * get single gallery by id
     * @param gallery
     * @returns {Promise<*|undefined>}
     */
    static async get(gallery) {
        const db = new DatabaseModel('galleries');
        return await db.aggregate([{
            $match: {
                _id: gallery
            }
        }]);
    }

    /**
     * save gallery details
     * @param name
     * @returns {Promise<*>}
     */
    static async set(name) {
        const db = new DatabaseModel('galleries');
        const slug = name.toLowerCase().replace(/[^\w\s]/gi, '').replace(/\s/g, '-');

        let doc = {
            _id: uuid.v1(),
            name,
            slug,
            created_at: new Date()
        };

        return await db.insert(doc);
    }

}

module.exports = GalleryController;