'use strict';

const uuid = require("uuid");
const DatabaseModel = require('./../models/MongoModel');
const ImageModel = require('./../models/ImageModel');
const Storage = require('./../models/GDriveModel');

class SnapshotController {

    /**
     * fetch list of snapshots by gallery id
     * @param gallery
     * @param filter
     * @returns {Promise<*|undefined>}
     */
    static async list(gallery, filter) {
        const db = new DatabaseModel('snapshots');
        return await db.aggregate([
            {$match: {gallery}}
        ]);
    }


    /**
     * get single snapshot by id
     * @param _id
     * @returns {Promise<*|undefined>}
     */
    static async get(_id) {
        const db = new DatabaseModel('snapshots');
        return await db.aggregate([
            {$match: {_id}}
        ]);
    }


    /**
     * set single snapshot
     * @param snapshot
     * @param body
     * @returns {Promise<*>}
     */
    static async set(snapshot, body) {
        const {gallery, filter} = body;
        let fileExt = snapshot.mimetype.split('/')[1];

        let metadata = {
            _id: uuid.v1(),
            file_name: `${Date.now()}.${fileExt}`,
            path: `/${gallery}`,
            mimetype: snapshot.mimetype,
            size: snapshot.size,
            gallery,
            filter,
            created_at: new Date()
        }

        // store image
        const image = new ImageModel();
        let uploadedImage = await image.upload(`/${metadata.gallery}`, metadata.file_name, snapshot);

        // store image external
        /*
        const storage = new Storage();
        let storedImage = await storage.upload(`/${metadata.gallery}/${metadata.file_name}`, metadata);
        metadata['external'] = storedImage;

        TODO:
        1. generate folder when creating a gallery
        2. store folder-id in gallery
        3. before upload file, check if folder exists
        4. upload image

        add environment vars in docker-compose file?
         */

        // store image details
        const db = new DatabaseModel('snapshots');
        let storedMetadata = await db.insert(metadata);

        return metadata;
    }

}

module.exports = SnapshotController;