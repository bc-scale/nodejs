'use strict';

const express = require('express');
const router = express.Router();

const GalleryController = require('./../controller/GalleryController');
const SnapshotController = require('./../controller/SnapshotController');

/**
 * Create new Gallery
 * {name: String}
 */
router.post(`/`,
    async (req, res) => {
        const {name} = req.body;

        try {
            let result = await GalleryController.set(name);

            res.status(200).json({
                success: true,
                result
            });
        } catch (e) {
            res.status(500).json({
                success: false,
                result: e
            });
        }
    });

router.get(`/list`,
    async (req, res) => {
        try {
            let result = await GalleryController.list();

            res.status(200).json({
                success: true,
                result
            });
        } catch (e) {
            res.status(500).json({
                success: false,
                result: e
            });
        }
    });

router.get(`/:gallery/:limit?`,
    async (req, res) => {
        const {gallery, limit} = req.params;

        try {
            let g = await GalleryController.get();
            let s = await SnapshotController.list(gallery, {limit});

            res.status(200).json({
                success: true,
                result: {
                    gallery: g,
                    snapshots: s
                }
            });
        } catch (e) {
            res.status(500).json({
                success: false,
                result: e
            });
        }
    });


module.exports = router;